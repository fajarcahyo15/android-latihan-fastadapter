package io.github.fajarcahyo15.latihan_fastadapter.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.materialize.holder.StringHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.fajarcahyo15.latihan_fastadapter.R;

public class Member extends AbstractItem<Member, Member.ViewHolder> implements Parcelable {
    private String uid;
    private String name;

    public Member() {
    }

    protected Member(Parcel in) {
        uid = in.readString();
        name = in.readString();
    }

    public static final Creator<Member> CREATOR = new Creator<Member>() {
        @Override
        public Member createFromParcel(Parcel in) {
            return new Member(in);
        }

        @Override
        public Member[] newArray(int size) {
            return new Member[size];
        }
    };

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    public Member(String uid, String name) {
        this.uid = uid;
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getType() {
        return R.id.member_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.adapter_member;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeString(name);
    }

    public static class ViewHolder extends FastAdapter.ViewHolder<Member> {

        @BindView(R.id.id)
        AppCompatTextView id;

        @BindView(R.id.name)
        AppCompatTextView name;

        @BindView(R.id.btn_aksi)
        Button btn_aksi;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindView(Member item, List<Object> payloads) {

            StringHolder.applyTo(new StringHolder(item.uid), id);
            StringHolder.applyTo(new StringHolder(item.name), name);
        }

        @Override
        public void unbindView(Member item) {
            id.setText(null);
            name.setText(null);
        }
    }
}
