package io.github.fajarcahyo15.latihan_fastadapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;
import com.mikepenz.fastadapter.listeners.OnClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.fajarcahyo15.latihan_fastadapter.model.Member;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.member_list)
    RecyclerView member_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final FastItemAdapter<Member> memberAdapter = new FastItemAdapter<>();
        memberAdapter.withSelectable(true);
        member_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        member_list.setAdapter(memberAdapter);
        member_list.setNestedScrollingEnabled(false);

        memberAdapter.withOnClickListener(new OnClickListener<Member>() {
            @Override
            public boolean onClick(View v, IAdapter<Member> adapter, Member item, int position) {
                Toast.makeText(MainActivity.this, item.getUid(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        memberAdapter.withEventHook(new ClickEventHook<Member>() {
            @Nullable
            @Override
            public View onBind(RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof Member.ViewHolder) {
                    return viewHolder.itemView.findViewById(R.id.btn_aksi);
                }
                return null;
            }


            @Override
            public void onClick(View v, int position, FastAdapter<Member> fastAdapter, Member item) {
                Toast.makeText(MainActivity.this, "Aksi: "+item.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        memberAdapter.add(new Member("123", "Pelaku A"));
        memberAdapter.add(new Member("456", "Pelaku B"));
        memberAdapter.add(new Member("789", "Pelaku C"));
    }
}
