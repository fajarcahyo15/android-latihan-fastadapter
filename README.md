## Latihan Menggunakan Fast Adapter

 - Konten:
	 - Menampilkan list data
	 - Menampilkan data list yang dipilih
	 - Reaksi terhadap komponen dalam list


 - Library:
	 - [ButterKnife 8.8.1](http://jakewharton.github.io/butterknife/)
	 - [FastAdapter 3.2.5](https://github.com/mikepenz/FastAdapter)